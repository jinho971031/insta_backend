import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = async (_, { commentId, lastID }, { client }) => {
  const likes = await client.commentLike.findMany({
    where: {
      commentId,
    },
    select: {
      user: true,
    },
    take: 5,
    skip: lastID ? 1 : 0,
    ...(lastID && { cursor: { id: lastID } }),
  });

  if (!likes) {
    throw new Error("Like is not exists.");
  }
  return likes.map((like) => like.user);
};

// 사진을 좋아요하는 사람 정보 가져오기

const resolvers: Resolvers = {
  Query: {
    seeCommentLikes: resolver,
  },
};

export default resolvers;
