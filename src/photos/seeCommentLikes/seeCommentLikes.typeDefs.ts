import { gql } from "apollo-server-core";

export default gql`
  type Query {
    seeCommentLikes(commentId: Int!, lastID: Int): [User]
  }
`;
