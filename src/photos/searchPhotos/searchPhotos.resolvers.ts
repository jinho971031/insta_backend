import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = (_, { keyword }, { client }) =>
  client.photo.findMany({
    where: {
      caption: {
        startsWith: keyword,
      },
    },
  });

const resolvers: Resolvers = {
  Query: {
    searchPhotos: resolver,
  },
};

export default resolvers;
