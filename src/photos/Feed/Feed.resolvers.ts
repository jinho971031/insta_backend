import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = (_, { lastID }, { client, loggedInUser }) =>
  // Feed에서 보여줄 사진 찾기 ( follower에 내가 들어있는 사람들의 사진 )
  client.photo.findMany({
    where: {
      OR: [
        {
          user: {
            followers: {
              some: {
                id: loggedInUser.id,
              },
            },
          },
        },
        {
          userId: loggedInUser.id,
        },
      ],
    },
    // 오름차순 정렬
    orderBy: {
      createdAt: "desc",
    },
    // 커서 페이징
    take: 20,
    skip: lastID ? 1 : 0,
    ...(lastID && { cursor: { id: lastID } }),
  });

const resolvers: Resolvers = {
  Query: {
    Feed: protectedResolver(resolver),
  },
};

export default resolvers;
