import { gql } from "apollo-server-core";

export default gql`
  type Query {
    Feed(lastID: Int): [Photo]
  }
`;
