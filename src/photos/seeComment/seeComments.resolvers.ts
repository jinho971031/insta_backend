import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = (_, { photoId, lastID }, { client }) =>
  client.comment.findMany({
    where: {
      photoId,
    },
    take: 20,
    skip: lastID ? 1 : 0, // 이미 봤던 것 제외 시켜
    ...(lastID && { cursor: { id: lastID } }),
  });

const resolvers: Resolvers = {
  Query: {
    seeComments: protectedResolver(resolver),
  },
};

export default resolvers;
