import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = async (_, { photoId, lastID }, { client }) => {
  const exists = await client.photo.findUnique({
    where: {
      id: photoId,
    },
    select: {
      id: true,
    },
  });

  if (!exists) {
    throw new Error("Photo is not exists.");
  }

  return client.comment.findMany({
    where: {
      photoId,
    },
    orderBy: {
      createdAt: "asc",
    },
    take: 20,
    skip: lastID ? 1 : 0,
    ...(lastID && { cursor: { id: lastID } }),
  });
};

const resolvers: Resolvers = {
  Query: {
    seePhotoComments: resolver,
  },
};

export default resolvers;
