import { gql } from "apollo-server-core";

export default gql`
  type Photo {
    id: Int!
    user: User!
    file: String!
    caption: String
    hashtags: [Hashtag]
    likes: [PhotoLike]
    comments: [Comment]
    createdAt: String!
    updatedAt: String!
    likeCount: Int!
    isLiked: Boolean!
    commentCount: Int!
    isMine: Boolean!
  }

  type Hashtag {
    id: Int!
    hashtag: String!
    photos(lastID: Int): [Photo]
    totalPhotos: Int!
    createdAt: String!
    updatedAt: String!
  }

  type PhotoLike {
    id: Int!
    photo: Photo!
    user: User!
    createdAt: String!
    updatedAt: String!
  }

  type Comment {
    id: Int!
    comment: String!
    photo: Photo!
    user: User!
    createdAt: String!
    updatedAt: String!
    likeCount: Int!
    isLiked: Boolean!
    isMine: Boolean!
  }

  type CommentLike {
    id: Int!
    comment: Comment!
    user: User!
    createdAt: String!
    updatedAt: String!
  }
`;
