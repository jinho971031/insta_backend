import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = async (
  _,
  { commentId: id, comment },
  { client, loggedInUser },
  info
) => {
  console.log(info);
  const exists = await client.comment.findUnique({
    where: {
      id,
    },
    select: {
      userId: true,
    },
  });

  if (!exists) {
    return {
      ok: false,
      error: "Comment is not exists.",
    };
  }

  if (exists.userId !== loggedInUser.id) {
    return {
      ok: false,
      error: "You don't have Permission.",
    };
  }

  await client.comment.update({
    where: {
      id,
    },
    data: {
      comment,
    },
  });

  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    editComment: protectedResolver(resolver),
  },
};

export default resolvers;
