import { deleteToS3 } from "../../shared/shared.utils";
import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = async (_, { photoId }, { client, loggedInUser }) => {
  const exists = await client.photo.findUnique({
    where: {
      id: photoId,
    },
    select: {
      userId: true,
      file: true,
    },
  });

  if (!exists) {
    return {
      ok: false,
      error: "Photo is not exists.",
    };
  } else if (exists.userId !== loggedInUser.id) {
    return {
      ok: false,
      error: "You don't have Permission.",
    };
  } else {
    // 코멘트 삭제
    await client.commentLike.deleteMany({
      where: {
        photoId,
      },
    });
    await client.comment.deleteMany({
      where: {
        photoId,
      },
    });

    // 사진 삭제
    await client.photoLike.deleteMany({
      where: {
        photoId,
      },
    });
    await client.photo.delete({
      where: {
        id: photoId,
      },
    });

    // 아마존 파일 지우기
    await deleteToS3(exists.file);

    //삭제 완료
    return {
      ok: true,
    };
  }
};

const resolvers: Resolvers = {
  Mutation: {
    deletePhoto: protectedResolver(resolver),
  },
};

export default resolvers;
