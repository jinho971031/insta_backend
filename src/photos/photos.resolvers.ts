import client from "../client";
import { Resolvers } from "../types";

const resolvers: Resolvers = {
  Photo: {
    user: ({ userId }) => client.user.findUnique({ where: { id: userId } }),
    hashtags: ({ id }) =>
      client.hashtag.findMany({ where: { photos: { some: { id } } } }),
    likes: ({ id }, { lastID }) =>
      client.photoLike.findMany({
        where: {
          photoId: id,
        },
        take: 20,
        skip: lastID ? 1 : 0,
        ...(lastID && { cursor: { id: lastID } }),
      }),
    comments: ({ id }, { lastID, take }) =>
      client.comment.findMany({
        where: {
          photoId: id,
        },
        take: take ? take : 20,
        skip: lastID ? 1 : 0,
        ...(lastID && { cursor: { id: lastID } }),
      }),
    isLiked: async ({ id }, _, { loggedInUser }) => {
      const exists = await client.photoLike.findUnique({
        where: {
          photoId_userId: {
            photoId: id,
            userId: loggedInUser.id,
          },
        },
      });
      return exists !== null;
    },
    likeCount: ({ id }) =>
      client.photoLike.count({
        where: {
          photoId: id,
        },
      }),
    commentCount: ({ id }) =>
      client.comment.count({
        where: {
          photoId: id,
        },
      }),
    isMine: ({ userId }, _, { loggedInUser }) => {
      if (!loggedInUser) {
        return false;
      }
      return userId === loggedInUser.id;
    },
  },
  Hashtag: {
    photos: ({ id }, { lastID }) =>
      client.hashtag
        .findUnique({
          where: {
            id,
          },
        })
        .photos({
          take: 3,
          skip: lastID ? 1 : 0,
          ...(lastID && { cursor: { id: lastID } }),
        }),

    totalPhotos: ({ id }) =>
      client.photo.count({
        where: {
          hashtags: {
            some: {
              id,
            },
          },
        },
      }),
  },
  PhotoLike: {
    user: ({ userId }) => client.user.findUnique({ where: { id: userId } }),
  },
  Comment: {
    user: ({ userId }) => client.user.findUnique({ where: { id: userId } }),
    isLiked: async ({ id }, _, { loggedInUser }) => {
      const exists = await client.commentLike.findFirst({
        where: {
          commentId: id,
          userId: loggedInUser.id,
        },
      });
      return exists !== null;
    },
    likeCount: ({ id }) =>
      client.commentLike.count({
        where: {
          commentId: id,
        },
      }),
    isMine: ({ userId }, _, { loggedInUser }) => {
      if (!loggedInUser) {
        return false;
      }
      return userId === loggedInUser.id;
    },
  },
  CommentLike: {
    user: ({ userId }) => client.user.findUnique({ where: { id: userId } }),
  },
};

export default resolvers;
