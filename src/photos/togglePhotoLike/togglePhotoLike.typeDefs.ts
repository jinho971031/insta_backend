import { gql } from "apollo-server-core";

export default gql`
  type Mutation {
    togglePhotoLike(photoId: Int!): MutationResult
  }
`;
