import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = async (_, { photoId }, { client, loggedInUser }) => {
  // 사진 있는지 확인
  const photo = await client.photo.findUnique({
    where: {
      id: photoId,
    },
    select: {
      id: true,
    },
  });

  if (!photo) {
    return {
      ok: false,
      error: "Photo is not exists",
    };
  }

  const likeWhere = {
    photoId_userId: {
      photoId,
      userId: loggedInUser.id,
    },
  };

  // 좋아요 눌렸는지 확인
  const like = await client.photoLike.findUnique({
    where: likeWhere,
  });

  if (like) {
    // 좋아요 눌렀으면 좋아요 취소하기
    await client.photoLike.delete({
      where: likeWhere,
    });
  } else {
    // 좋아요 안눌렀으면 좋아요 만들기
    await client.photoLike.create({
      data: {
        user: {
          connect: {
            id: loggedInUser.id,
          },
        },
        photo: {
          connect: {
            id: photoId,
          },
        },
      },
    });
  }

  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    togglePhotoLike: protectedResolver(resolver),
  },
};

export default resolvers;
