import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = (_, { id }, { client }) =>
  client.photo.findUnique({
    where: {
      id,
    },
  });

const resolvers: Resolvers = {
  Query: {
    seePhoto: resolver,
  },
};

export default resolvers;
