import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = async (
  _,
  { commentId },
  { client, loggedInUser }
) => {
  const exists = await client.comment.findUnique({
    where: {
      id: commentId,
    },
    select: {
      userId: true,
    },
  });

  if (!exists) {
    return {
      ok: false,
      error: "Photo is not exists.",
    };
  } else if (exists.userId !== loggedInUser.id) {
    return {
      ok: false,
      error: "You don't have Permission.",
    };
  } else {
    await client.commentLike.deleteMany({
      where: {
        commentId,
      },
    });
    await client.comment.delete({
      where: {
        id: commentId,
      },
    });

    return {
      ok: true,
    };
  }
};

const resolvers: Resolvers = {
  Mutation: {
    deleteComment: protectedResolver(resolver),
  },
};

export default resolvers;
