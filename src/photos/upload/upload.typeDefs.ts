import { gql } from "apollo-server-core";

export default gql`
  type Mutation {
    upload(file: Upload!, caption: String): Photo
  }
`;
