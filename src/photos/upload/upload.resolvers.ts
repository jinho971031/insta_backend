import { uploadToS3 } from "../../shared/shared.utils";
import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";
import { processHashtags } from "../photos.utils";

const resolver: Resolver = async (
  _,
  { file, caption },
  { loggedInUser, client }
) => {
  // 파일 업로드
  const fileUrl = await uploadToS3(file, loggedInUser.id, "uploads");
  // 사진 추가
  return client.photo.create({
    data: {
      file: fileUrl,
      caption,
      user: {
        connect: {
          id: loggedInUser.id,
        },
      },
      hashtags: {
        connectOrCreate: processHashtags(caption),
      },
    },
  });
};

const resolvers: Resolvers = {
  Mutation: {
    upload: protectedResolver(resolver),
  },
};

export default resolvers;
