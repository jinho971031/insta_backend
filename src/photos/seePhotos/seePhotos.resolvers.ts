import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = (_, { userId }, { client }) =>
  client.photo.findMany({
    where: {
      userId,
    },
  });

const resolvers: Resolvers = {
  Query: {
    seePhotos: resolver,
  },
};

export default resolvers;
