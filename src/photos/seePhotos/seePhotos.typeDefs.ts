import { gql } from "apollo-server-express";

export default gql`
  type Query {
    seePhotos(userId: Int!): [Photo]
  }
`;
