import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = (_, { hashtag }, { client }) =>
  client.hashtag.findUnique({
    where: {
      hashtag,
    },
  });

const resolvers: Resolvers = {
  Query: {
    seeHashtag: resolver,
  },
};

export default resolvers;
