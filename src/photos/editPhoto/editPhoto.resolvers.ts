import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";
import { processHashtags } from "../photos.utils";

const resolver: Resolver = async (
  _,
  { id, caption },
  { loggedInUser, client }
) => {
  const oldPhoto = await client.photo.findFirst({
    where: {
      id,
      userId: loggedInUser.id,
    },
    include: {
      hashtags: {
        select: {
          hashtag: true,
        },
      },
    },
  });

  if (!oldPhoto) {
    return {
      ok: false,
      error: "Photo is not exists.",
    };
  }

  if (oldPhoto.userId !== loggedInUser.id) {
    return {
      ok: false,
      error: "You don't have Permission.",
    };
  }

  await client.photo.update({
    where: {
      id,
    },
    data: {
      caption,
      hashtags: {
        disconnect: oldPhoto.hashtags,
        connectOrCreate: processHashtags(caption),
      },
    },
  });

  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    editPhoto: protectedResolver(resolver),
  },
};

export default resolvers;
