import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";
import seePhotoLikesTypeDefs from "../seePhotoLikes/seePhotoLikes.typeDefs";

const resolver: Resolver = async (
  _,
  { commentId },
  { client, loggedInUser }
) => {
  // 댓글 있는지 확인
  const comment = await client.comment.findUnique({
    where: {
      id: commentId,
    },
    select: {
      photoId: true,
    },
  });

  if (!comment) {
    return {
      ok: false,
      error: "댓글이 존재하지 않습니다.",
    };
  }

  const likeWhere = {
    commentId,
    userId: loggedInUser.id,
  };

  // 좋아요 눌렸는지 확인
  const like = await client.commentLike.findFirst({
    where: likeWhere,
    select: {
      id: true,
    },
  });

  if (like) {
    // 좋아요 눌렀으면 좋아요 취소하기
    await client.commentLike.delete({
      where: {
        id: like.id,
      },
    });
  } else {
    // 좋아요 안눌렀으면 좋아요 만들기
    await client.commentLike.create({
      data: {
        user: {
          connect: {
            id: loggedInUser.id,
          },
        },
        photo: {
          connect: {
            id: comment.photoId,
          },
        },
        comment: {
          connect: {
            id: commentId,
          },
        },
      },
    });
  }

  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    toggleCommentLike: protectedResolver(resolver),
  },
};

export default resolvers;
