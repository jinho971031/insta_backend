import { gql } from "apollo-server-core";

export default gql`
  type Mutation {
    toggleCommentLike(commentId: Int!): MutationResult
  }
`;
