import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = async (
  _,
  { photoId: id, comment },
  { client, loggedInUser }
) => {
  // 사진의 존재 유무만 알면 되므로, select로 id만 가져온다.
  const exists = await client.photo.findUnique({
    where: {
      id,
    },
    select: {
      id: true,
    },
  });

  if (!exists) {
    return {
      ok: false,
      error: "Photo is not exists",
    };
  }

  const newComment = await client.comment.create({
    data: {
      comment,
      user: {
        connect: {
          id: loggedInUser.id,
        },
      },
      photo: {
        connect: {
          id,
        },
      },
    },
  });

  return {
    ok: true,
    id: newComment.id,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    createComment: protectedResolver(resolver),
  },
};

export default resolvers;
