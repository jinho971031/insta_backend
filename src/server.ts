require("dotenv").config();

import express from "express";
import logger from "morgan";
import http from "http";

import { ApolloServer } from "apollo-server-express";
import { typeDefs, resolvers } from "./schema";
import { getUser } from "./users/users.utils";
import client from "./client";
import pubsub from "./ pubsub";

console.log(pubsub);

const apollo = new ApolloServer({
  resolvers,
  typeDefs,
  context: async (ctx) => {
    if (ctx.req) {
      return {
        loggedInUser: await getUser(ctx.req.headers.authorization),
        client,
      };
    }
    return ctx.connection.context;
  },
  subscriptions: {
    onConnect: async (params: any) => {
      const token = params.authorization;
      // const token = params["Authorization"];
      if (!token) {
        throw new Error("You can't listen.");
      }

      return {
        loggedInUser: await getUser(token),
        client,
      };
    },
  },
});

// express 서버 추가
const app = express();
// app.use(logger("tiny"));

// static이란 이름으로 upload 폴더 접근 가능
app.use("/static", express.static("uploads"));

// Apollo 서버에 express 서버 추가
apollo.applyMiddleware({ app });

const httpServer = http.createServer(app);

apollo.installSubscriptionHandlers(httpServer);

const PORT = process.env.PORT;

// httpServer를 감사히겠다. ( app 말고 )
httpServer.listen({ port: PORT }, () =>
  console.log("🚀 Server is running on https://localhost:4000")
);
