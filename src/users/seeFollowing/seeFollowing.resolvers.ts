import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = async (_, { username, lastID }, { client }) => {
  const exists = await client.user.findUnique({
    where: { username },
    select: { id: true },
  });

  // username 존재하지 않을때 에러메시지 보여줌
  if (!exists) {
    return {
      ok: false,
      error: "Username is not exists",
    };
  }

  const following = await client.user
    .findUnique({
      where: {
        username,
      },
    })
    .following({
      take: 5,
      skip: lastID ? 1 : 0,
      // 마지막으로 봤던 것 커서로 지정하여, 다음 부분부터 표시하도록 만들기
      ...(lastID && { cursor: { id: lastID } }),
    });

  // following이 존재하지 않으면 에러메시지 보여줌
  if (!following) {
    return {
      ok: false,
      error: "following is not exist.",
    };
  }

  return {
    ok: true,
    following,
  };
};

const resolvers: Resolvers = {
  Query: {
    seeFollowing: resolver,
  },
};

export default resolvers;
