import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = (_, { keyword, lastID }, { client }) =>
  client.user.findMany({
    where: {
      OR: [
        {
          username: {
            startsWith: keyword.toLowerCase(),
          },
        },
        {
          name: {
            startsWith: keyword.toLowerCase(),
          },
        },
      ],
    },
    take: 5,
    skip: lastID ? 1 : 0,
    ...(lastID && { cursor: { id: lastID } }),
  });

const resolvers: Resolvers = {
  Query: {
    searchUsers: resolver,
  },
};

export default resolvers;
