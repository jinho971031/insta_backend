import { gql } from "apollo-server";

export default gql`
  type User {
    id: Int!
    name: String!
    username: String!
    email: String!
    bio: String
    avatar: String
    totalPhotos: Int!
    photos(lastID: Int): [Photo]
    following: [User]
    followers: [User]
    likes: PhotoLike
    createdAt: String!
    updatedAt: String!
    totalFollowing: Int!
    totalFollowers: Int!
    isFollowing: Boolean!
    isMe: Boolean!
  }
`;
