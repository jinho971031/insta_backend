import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = (_, { username }, { client }) =>
  client.user.findUnique({
    where: { username },
    // prisma 에게 관련된 정보 가져오라고 알려주기
    include: {
      following: true,
      followers: true,
    },
  });

const resolvers: Resolvers = {
  Query: {
    seeProfile: resolver,
  },
};

export default resolvers;
