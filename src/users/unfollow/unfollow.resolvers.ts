import client from "../../client";
import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../users.utils";

const resolver: Resolver = async (_, { username }, { loggedInUser }) => {
  const exists = await client.user.findUnique({
    where: { username },
  });

  // username 존재하지 않으면 에러메시지 보여줌
  if (!exists) {
    return {
      ok: false,
      error: "Username is not exist.",
    };
  }

  await client.user.update({
    where: {
      id: loggedInUser.id,
    },
    data: {
      following: {
        disconnect: {
          username,
        },
      },
    },
  });

  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    unfollow: protectedResolver(resolver),
  },
};

export default resolvers;
