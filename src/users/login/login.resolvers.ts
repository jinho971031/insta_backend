import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = async (_, { username, password }, { client }) => {
  // username 찾기
  const user = await client.user.findUnique({
    where: {
      username,
    },
  });

  // user 없으면 에러메시지 출력
  if (!user) {
    return {
      ok: false,
      error: "아이디 비밀번호를 확인해주세요.",
    };
  }

  // 비밀번호 체크하기
  const passwordOk = await bcrypt.compare(password, user.password);

  // 비밀번호 일치하지 않으면 에러메시지
  if (!passwordOk) {
    return {
      ok: false,
      error: "아이디 비밀번호를 확인해주세요.",
    };
  }

  // 로그인 토큰 만들어서 돌려줌
  const token = await jwt.sign({ id: user.id }, process.env.SECRET_KEY);
  return {
    ok: true,
    token,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    login: resolver,
  },
};

export default resolvers;
