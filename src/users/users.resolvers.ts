import { Resolvers } from "../types";

const resolvers: Resolvers = {
  User: {
    totalFollowing: ({ id }, _, { client }) =>
      client.user.count({
        where: {
          followers: {
            some: {
              id,
            },
          },
        },
      }),

    totalFollowers: ({ id }, _, { client }) =>
      client.user.count({
        where: {
          following: {
            some: {
              id,
            },
          },
        },
      }),

    isFollowing: async ({ id }, _, { loggedInUser, client }) => {
      if (!loggedInUser) {
        return false;
      }

      if (id === loggedInUser.id) {
        return false;
      }

      // 이름이 현재 사용 유저이며, 동시에 id를 팔로우하는 유저가 몇명있는지 세기
      const exists = await client.user.count({
        where: {
          username: loggedInUser.username,
          following: {
            some: {
              id,
            },
          },
        },
      });

      // 일치하는 유저가 없으면 0, 있으면 1 반환하고, 이를 Boolean 값으로 변경하여 준다.
      return Boolean(exists);
    },

    isMe: ({ id }, _, { loggedInUser }) => {
      if (!loggedInUser) {
        return false;
      }
      return id === loggedInUser.id;
    },

    totalPhotos: ({ id: userId }, _, { client }) =>
      client.photo.count({
        where: {
          userId,
        },
      }),

    // pagination, 유저에 해당하는 사진 가져오기
    photos: ({ id }, { lastID }, { client }) =>
      client.user
        .findUnique({
          where: {
            id,
          },
        })
        .photos({
          take: 3,
          skip: lastID ? 1 : 0,
          ...(lastID && { cursor: { id: lastID } }),
        }),
  },
};

export default resolvers;
