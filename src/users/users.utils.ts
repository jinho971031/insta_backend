import jwt from "jsonwebtoken";
import client from "../client";
import { Context, Resolver } from "../types";

// 우리가 발급한 토큰인지 확인하는 기능
export const getUser = async (token: String) => {
  if (!token) {
    return {
      ok: false,
      error: "Token not exists.",
    };
  }
  const verifiedToken: any = await jwt.verify(token, process.env.SECRET_KEY);
  if ("id" in verifiedToken) {
    const user = await client.user.findUnique({
      where: { id: verifiedToken["id"] },
    });
    if (user) {
      return user;
    }
  }
  return {
    ok: false,
    error: "Token not varified.",
  };
};

// resolver 접근시, 로그인 상태 확인하는 기능
export const protectedResolver =
  (ourResolver: Resolver) =>
  (root: any, args: any, context: Context, info: any) => {
    if (!context.loggedInUser) {
      const operation = info.operation.operation;
      if (operation === "query") {
        console.log("not permission");
        return null;
      } else {
        return {
          ok: false,
          error: "Please log in to perform this action",
        };
      }
    }
    console.log(info.fieldName);

    return ourResolver(root, args, context, info);
  };
