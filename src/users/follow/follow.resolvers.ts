import client from "../../client";
import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../users.utils";

const resolver: Resolver = async (_, { username }, { loggedInUser }) => {
  const exists = await client.user.findFirst({
    where: {
      username,
    },
  });

  // username 존재하지 않으면 에러메시지 보여줌
  if (!exists) {
    return {
      ok: false,
      error: "Username is not exist.",
    };
  }

  await client.user.update({
    where: {
      id: loggedInUser.id,
    },
    data: {
      following: {
        // connect 는 unique한 데이터를 사용해야 연결가능하다
        connect: {
          username,
        },
      },
    },
  });

  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    follow: protectedResolver(resolver),
  },
};

export default resolvers;
