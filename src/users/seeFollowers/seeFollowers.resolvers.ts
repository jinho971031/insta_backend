import { Resolver, Resolvers } from "../../types";

const resolver: Resolver = async (_, { username, page }, { client }) => {
  const exists = await client.user.findUnique({
    where: { username },
    select: { id: true },
  });

  // username 존재하지 않을때 에러메시지 보여줌
  if (!exists) {
    return {
      ok: false,
      error: "Username is not exists",
    };
  }

  const followers = await client.user
    .findUnique({
      where: { username },
    })
    .followers({
      // 1페이지 마다 5개 씩 보여주기
      skip: (page - 1) * 5,
      take: 5,
    });

  // followers 존재하지 않을때, 에러메시지 보여줌
  if (!followers) {
    return {
      ok: true,
      error: "followers is not exist.",
    };
  }

  const totalFollowers = await client.user.count({
    where: {
      following: {
        every: {
          username,
        },
      },
    },
  });

  return {
    ok: true,
    followers,
    totalPages: Math.ceil(totalFollowers / 5),
  };
};

const resolvers: Resolvers = {
  Query: {
    seeFollowers: resolver,
  },
};

export default resolvers;
