import bcrypt from "bcrypt";
import client from "../../client";
import { protectedResolver } from "../users.utils";
import { createWriteStream } from "fs";
import { Resolver, Resolvers } from "../../types";
import { uploadToS3 } from "../../shared/shared.utils";

const resolver: Resolver = async (
  _,
  { name, username, email, password: newPassword, bio, avatar },
  { loggedInUser }
) => {
  const exists = client.user.findUnique({
    where: {
      id: loggedInUser.id,
    },
  });

  // 유저 존재하지 않으면 에러 메시지 보여줌
  if (!exists) {
    return {
      ok: false,
      error: "Could not update Profile.",
    };
  }

  // avatar 존재시, 이미지와 url 저장하기
  let avatarUrl = null;
  if (avatar) {
    avatarUrl = await uploadToS3(avatar, loggedInUser.id, "profiles");
    // const { filename, createReadStream } = await avatar;
    // const newFilename = `${loggedInUser.id}-${Date.now()}-${filename}`;
    // const readStream = createReadStream();
    // const writeStream = createWriteStream(
    //   process.cwd() + "/uploads/" + newFilename
    // );
    // readStream.pipe(writeStream);
    // avatarUrl = `http://localhost:4000/static/${newFilename}`;
  }

  // password 존재시, bcrypt 이용해서 암호화하고 저장하기
  let uglyPassword = null;
  if (newPassword) {
    uglyPassword = await bcrypt.hash(newPassword, 10);
  }

  // 정보 업데이트 (prisma가 undefined 데이터를 자동으로 걸러줌)
  await client.user.update({
    where: {
      id: loggedInUser.id,
    },
    data: {
      name,
      username,
      email,
      // uglyPassword가 존재하면, password: uglyPassword를 넣어준다.
      ...(uglyPassword && { password: uglyPassword }),
      bio,
      ...(avatarUrl && { avatar: avatarUrl }),
    },
  });
  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    editProfile: protectedResolver(resolver),
  },
};

export default resolvers;
