import { gql } from "apollo-server-core";

export default gql`
  type Mutation {
    editProfile(
      name: String
      username: String
      email: String
      password: String
      bio: String
      avatar: Upload
    ): MutationResult
  }
`;
