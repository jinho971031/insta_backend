import bcrypt from "bcrypt";
import client from "../../client";

export default {
  Mutation: {
    createAccount: async (_, { name, email, username, password }) => {
      // username, email이 존재하는지 체크하기
      // findFirst() 조건에 맞는 가장 첫번째 User가져옴
      const exists = await client.user.findFirst({
        where: {
          OR: [
            {
              username,
            },
            {
              email,
            },
          ],
        },
      });

      // 존재하면 에러 메시지 반환
      if (exists) {
        return {
          ok: false,
          error: "이미 존재하는 유저입니다.",
        };
      }
      // 암호 hash화
      const uglyPassword = await bcrypt.hash(password, 10);

      // 암호화된 비밀번호 저장
      await client.user.create({
        data: {
          name,
          username,
          email,
          password: uglyPassword,
        },
      });

      return {
        ok: true,
      };
    },
  },
};
