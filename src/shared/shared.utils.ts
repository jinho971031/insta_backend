import AWS from "aws-sdk";

AWS.config.update({
  credentials: {
    accessKeyId: process.env.AWS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
  },
});

export const uploadToS3 = async (file, userId: number, box?: string) => {
  const { filename, createReadStream } = await file;
  const readStream = createReadStream();

  const objectName = box
    ? `${box}/${userId}-${Date.now()}-${filename}`
    : `${userId}-${Date.now()}-${filename}`;
  const { Location } = await new AWS.S3()
    .upload({
      Bucket: process.env.BUCKET,
      Key: objectName,
      ACL: "public-read",
      Body: readStream,
    })
    .promise();
  return Location;
};

export const deleteToS3 = async (fileUrl) => {
  const Key = fileUrl.replace(
    `https://${process.env.BUCKET}.s3.ap-northeast-2.amazonaws.com/`,
    ""
    // 폴더 이름, 파일 이름만 빼고 지우기
  );
  await new AWS.S3()
    .deleteObject({
      Bucket: process.env.BUCKET,
      Key,
    })
    .promise();
};
