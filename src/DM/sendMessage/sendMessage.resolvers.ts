import pubsub from "../../ pubsub";
import { NEW_MESSAGE } from "../../constants";
import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = async (
  _,
  { message, roomId, userId },
  { client, loggedInUser }
) => {
  if (!roomId && !userId) {
    return {
      ok: false,
      error: "Please input room or user.",
    };
  }

  // 유저 검사
  if (userId) {
    if (userId === loggedInUser.id) {
      return {
        ok: false,
        error: "Can't send Message for me.",
      };
    }
    const user = await client.user.findUnique({
      where: {
        id: userId,
      },
      select: {
        id: true,
      },
    });

    // 유저 없으면 에러
    if (!user) {
      return {
        ok: false,
        error: "User is not exists.",
      };
    }

    // 룸있는지 확인
    const exists = await client.room.findFirst({
      where: {
        AND: [
          {
            users: {
              some: {
                id: userId,
              },
            },
          },
          {
            users: {
              some: {
                id: loggedInUser.id,
              },
            },
          },
        ],
      },
      select: {
        id: true,
      },
    });

    // 룸 있으면 에러
    if (exists) {
      console.log(exists);
      return {
        ok: false,
        error: "Room already exists!",
      };
    }

    // 상대방이 나를 팔로우하고 있는지 확인
    const following = await client.user.count({
      where: {
        id: userId,
        following: {
          some: {
            id: loggedInUser.id,
          },
        },
      },
    });

    // 상대방이 나를 팔로우하지 않으면 대화할 수 없음
    if (!following) {
      return {
        ok: false,
        error: "Can't send Message for Unfollowed user.",
      };
    }

    // 내가 상대방을 팔로우하고 있는지 확인
    const followers = await client.user.count({
      where: {
        id: loggedInUser.id,
        followers: {
          some: {
            id: userId,
          },
        },
      },
    });

    // 내가 상대방을 팔로우하지 않으면 대화할 수 없음
    if (!followers) {
      return {
        ok: false,
        error: "Can't send Message for Unfollowing user.",
      };
    }

    // 방만들기
    const room = await client.room.create({
      data: {
        users: {
          connect: [
            {
              id: userId,
            },
            {
              id: loggedInUser.id,
            },
          ],
        },
      },
    });

    roomId = room.id;
  }

  // 방 검사
  if (roomId) {
    // 방 검색
    const room = await client.room.findUnique({
      where: {
        id: roomId,
      },
      select: {
        id: true,
      },
    });

    // 없으면 에러
    if (!room) {
      return {
        ok: false,
        error: "Room is not exists.",
      };
    }
  }

  // 메세지 보내기
  const newMessage = await client.message.create({
    data: {
      message,
      room: {
        connect: {
          id: roomId,
        },
      },
      user: {
        connect: {
          id: loggedInUser.id,
        },
      },
    },
  });

  // 보낸 메세지 정보를 사용해 실시간으로 방 정보 변경
  pubsub.publish(NEW_MESSAGE, { roomUpdates: newMessage });
  // 결과
  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Mutation: {
    sendMessage: protectedResolver(resolver),
  },
};

export default resolvers;
