import { gql } from "apollo-server-core";

export default gql`
  type Room {
    id: Int!
    users: [User]!
    messages(lastID: Int): [Message]
    unreadTotal: Int!
    createdAt: String!
    updatedAt: String!
  }

  type Message {
    id: Int!
    message: String!
    user: User!
    room: Room!
    read: Boolean!
    createdAt: String!
    updatedAt: String!
    isMine: Boolean!
  }
`;
