import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = async (_, { roomId }, { client, loggedInUser }) => {
  const room = await client.room.findFirst({
    where: {
      id: roomId,
      users: {
        some: {
          id: loggedInUser.id,
        },
      },
    },
  });

  if (!room) {
    return null;
  }

  // 읽기 표시
  await client.message.updateMany({
    where: {
      roomId,
      userId: {
        not: loggedInUser.id,
      },
      read: false,
    },
    data: {
      read: true,
    },
  });

  return room;
};

const resolvers: Resolvers = {
  Query: {
    seeRoom: protectedResolver(resolver),
  },
};

export default resolvers;
