import { Resolver, Resolvers } from "../../types";
import { protectedResolver } from "../../users/users.utils";

const resolver: Resolver = async (_, { messageId }, { client }) => {
  const message = await client.message.findUnique({
    where: {
      id: messageId,
    },
    select: {
      id: true,
    },
  });

  if (!message) {
    return {
      ok: false,
      error: "Message is not exists.",
    };
  }

  await client.message.update({
    where: {
      id: messageId,
    },
    data: {
      read: true,
    },
  });

  return {
    ok: true,
  };
};

const resolvers: Resolvers = {
  Query: {
    seeMessage: protectedResolver(resolver),
  },
};

export default resolvers;
