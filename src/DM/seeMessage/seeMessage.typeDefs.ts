import { gql } from "apollo-server-core";

export default gql`
  type Query {
    seeMessage(messageId: Int!): MutationResult
  }
`;
