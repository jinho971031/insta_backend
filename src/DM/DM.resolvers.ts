import client from "../client";

export default {
  Room: {
    users: ({ id }) =>
      client.room
        .findUnique({
          where: {
            id,
          },
        })
        .users(),
    messages: ({ id, lastID }) =>
      client.message.findMany({
        where: {
          roomId: id,
        },
        take: 20,
        skip: lastID ? 1 : 0,
        ...(lastID && { cursor: { id: lastID } }),
      }),
    unreadTotal: async ({ id }, _, { loggedInUser }) => {
      if (!loggedInUser) {
        return 0;
      }
      return client.message.count({
        where: {
          roomId: id,
          userId: {
            not: loggedInUser.id,
          },
          read: false,
        },
      });
    },
  },
  Message: {
    user: ({ userId }) =>
      client.user.findUnique({
        where: {
          id: userId,
        },
      }),
    isMine: ({ userId }, _, { loggedInUser }) => {
      if (userId === loggedInUser.id) {
        return true;
      }
      return false;
    },
  },
};
