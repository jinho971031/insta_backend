import { withFilter } from "graphql-subscriptions";
import pubsub from "../../ pubsub";
import { NEW_MESSAGE } from "../../constants";

export default {
  Subscription: {
    roomUpdates: {
      subscribe: async (root, args, context, info) => {
        const room = await context.client.room.findFirst({
          where: {
            id: args.roomId,
            users: {
              some: {
                id: context.loggedInUser.id,
              },
            },
          },
          select: {
            id: true,
          },
        });

        if (!room) {
          throw new Error("You shall not see this.");
        }

        return withFilter(
          () => pubsub.asyncIterator(NEW_MESSAGE),
          async ({ roomUpdates }, { roomId }) => {
            return roomUpdates.roomId === roomId;
          }
        )(root, args, context, info); // withFilter를 함수형식으로 전해주기
      },
    },
  },
};
